import { Controller, Get } from '@nestjs/common';
import { PlanetService } from './planet.service';
import { Planet } from './planet.schema';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Planets')
@Controller()
export class PlanetController {
  constructor(private readonly planetService: PlanetService) {}

  @ApiOkResponse({
    description: 'Return list of all planets addresses in database',
    type: Planet,
  })
  @Get('/planets')
  async getPlanets(): Promise<Array<Planet>> {
    return this.planetService.findAllPlanets();
  }
}
