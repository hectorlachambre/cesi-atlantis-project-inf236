# Atlantis REST API

Welcome to the Atlantis REST API project! This fictional project is inspired by the Stargate universe, where the city of Atlantis is a hub of advanced technology and interstellar travel. Our application provides access to the Atlantis database, allowing users to retrieve information about Stargates and their addresses.

## Project Context

The Atlantis REST API is currently under development by the scientific teams at Atlantis. As we explore distant galaxies and uncover ancient secrets, our goal is to create a reliable and efficient API for managing data collected in the Pegasus galaxy. While the project is still in its early stages, we've already implemented a basic endpoint to retrieve Stargate addresses from our MongoDB database.

## Technical Overview

- **Framework**: NestJS v10
- **API Specification**: OpenAPI 3 (OAS3)
- **Database**: MongoDB v7.0

## Getting Started

### Install dependencies

Run `npm i` in local development to install NodeJS dependencies. If you are installing the app on a CI environment prefer to use `npm ci`. you can also change npm cache directory to your working directory as following

```bash
npm ci --cache .npm --prefer-offline
```

## Configuration

Some configuration is required to launch the application. You have to set up the DB connection as with the following `.env.development.local` example :

```ini
MONGO_URI=mongodb://localhost:27017/
MONGO_DBNAME=atlantis
MONGO_USERNAME=user
MONGO_PASSWORD=password
```

You can provide following environment files :

- `.env.development.local`
- `.env.development`
- `.env`
- `.env.production`

Or you can define these environment variables as real variables.

## Development server

Run `npm start` or `npm start:dev` for a dev server. Navigate to `http://localhost:3000/`. The application will automatically reload if you change any of the source files if you start in dev mode.

### Build

To build the application, run the following command:

```bash
npm run build
```

Compiled files are generated in `/dist` folder

### Unit Testing

We take testing seriously at Atlantis. Our unit tests ensure the reliability of our codebase. To run the tests and generate a JUnit report, use:

```bash
npm test
```

The test report will be available in the `/reports` directory.

### Packaging

#### Application Packaging

To package the application for deployment, use the following command:

```bash
npm pack
```

This will create a tarball containing the compiled code and necessary dependencies.

#### Publishing

Before publishing, make sure you have set up your npm registry credentials. Create a `.npmrc` file in the project root with the following content (example with Gitlab Package Registry):

```ini
@scope:registry=https://gitlab.com/api/v4/projects/your_project_id/packages/npm/
//gitlab.com/api/v4/projects/your_project_id/packages/npm/:_authToken="${NPM_TOKEN}"
```

Replace `NPM_TOKEN` with the right access token

Now you're ready to publish the package:

```bash
npm publish
```

## Contributing

We welcome contributions from fellow explorers! Feel free to submit pull requests or open issues. Let's continue our journey in the Pegasus galaxy together! 🚀🌌
